import { Panel, PrimaryButton, Text, Link } from '@fluentui/react';
import { Component, Fragment, h } from 'preact';

export default class AboutPane extends Component<{}, { visible: boolean }> {
  state = {
    visible: false
  };

  constructor() {
    super();
  }

  onHashChange = () => {
    let hash = window.location.hash;
    if (hash.startsWith('#')) hash = hash.substring(1);
    this.setState({ visible: hash.toLowerCase() === 'about' });
  }

  componentDidMount() {
    window.addEventListener('hashchange', this.onHashChange);
    this.onHashChange();
  }

  componentWillUnmount() {
    window.removeEventListener('hashchange', this.onHashChange);
  }

  onDismiss = () => {
    window.history.back();
  }

  renderFooter = () => {
    return (
      <>
        <PrimaryButton onClick={this.onDismiss}>Close</PrimaryButton>
      </>
    );
  }

  render() {
    return (
      <Panel
        headerText='About'
        isOpen={this.state.visible}
        onDismiss={this.onDismiss}
        onRenderFooterContent={this.renderFooter}
      >
        <>
          <p><Text variant='medium'>This webapp was created by <Link href='https://qlcvea.com' target='_blank'>Marco (qlcvea)</Link>.</Text></p>
          <p><Text variant='medium'>This webapp is <Link href='https://gitlab.com/nast.ro/urlencode' target='_blank'>open source on GitLab</Link>.</Text></p>
          <p><Text variant='medium'><Link href='assets/licenses.txt' target='_blank'>Open source licenses</Link></Text></p>
        </>
      </Panel>
    );
  }
}