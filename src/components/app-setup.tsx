import { Checkbox, Panel, PrimaryButton, ChoiceGroup, mergeStyles, useTheme, IChoiceGroupOption } from '@fluentui/react';
import { Component, Fragment, h } from 'preact';
import globalState from '../global-state';

export default class AppSetup extends Component<{}, { visible: boolean }> {
  state = {
    visible: false
  };

  constructor() {
    super();
  }

  onHashChange = () => {
    let hash = window.location.hash;
    if (hash.startsWith('#')) hash = hash.substring(1);
    this.setState({ visible: hash.toLowerCase() === 'setup' });
  }

  componentDidMount() {
    window.addEventListener('hashchange', this.onHashChange);
    this.onHashChange();
  }

  componentWillUnmount() {
    window.removeEventListener('hashchange', this.onHashChange);
  }

  onAutoEncodeChange = (_: any, checked: boolean | undefined) => {
    if (typeof checked === 'boolean') {
      globalState.autoEncode = checked;
      window.localStorage.setItem('auto-encode', checked ? 'true' : 'false');
    }
    this.setState({});
  }

  onSaveContentChange = (_: any, checked: boolean | undefined) => {
    if (typeof checked === 'boolean') {
      globalState.saveContent = checked;
      window.localStorage.setItem('save-content', checked ? 'true' : 'false');
      if (!checked) {
        window.localStorage.removeItem('source');
        window.localStorage.removeItem('encoded');
      }
    }
    this.setState({});
  }

  onEncodeSpaceAsPlusChange = (_: any, option: IChoiceGroupOption | undefined) => {
    if (typeof option === 'object') {
      globalState.encodeSpaceAsPlus = option.key === 'true';
      window.localStorage.setItem('encode-space-as-plus', globalState.encodeSpaceAsPlus ? 'true' : 'false');
    }
    this.setState({});
  }

  onDismiss = () => {
    window.history.back();
  }

  renderFooter = () => {
    return (
      <>
        <PrimaryButton onClick={this.onDismiss}>Close</PrimaryButton>
      </>
    );
  }

  render() {
    const theme = useTheme();

    const checkboxStyle = mergeStyles(theme, {
      paddingTop: 10,
      paddingBottom: 10
    });

    return (
      <Panel
        headerText='Settings'
        isOpen={this.state.visible}
        onDismiss={this.onDismiss}
        onRenderFooterContent={this.renderFooter}
      >
        <>
          <Checkbox label="Instant encode/decode" checked={globalState.autoEncode} onChange={this.onAutoEncodeChange} className={checkboxStyle} />
          <Checkbox label="Save content" checked={globalState.saveContent} onChange={this.onSaveContentChange} className={checkboxStyle} />
          <ChoiceGroup
            selectedKey={globalState.encodeSpaceAsPlus ? 'true' : 'false'}
            onChange={this.onEncodeSpaceAsPlusChange}
            options={[
              { key: 'false', text: '%20' },
              { key: 'true', text: '+' },
            ]}
            label='Encode spaces as'
          />
        </>
      </Panel>
    );
  }
}