import { mergeStyles, PrimaryButton, useTheme, Text, DefaultButton, MessageBar, MessageBarType } from '@fluentui/react';
import monaco from 'monaco-editor';
import Editor from '@monaco-editor/react';
import { Component, Fragment, h, createRef, RefObject } from 'preact';

import Header from './header';
import PrivacySetup from './privacy-setup';
import AppSetup from './app-setup';
import globalState from '../global-state';
import AboutPane from './about-pane';

export default class App extends Component<{}, { hash: string, error: string | null }> {
  state = {
    hash: '',
    error: null
  };

  _sourceEditorRef = createRef<monaco.editor.IStandaloneCodeEditor>();
  _encodedEditorRef = createRef<monaco.editor.IStandaloneCodeEditor>();
  _autoEditing: 'source' | 'encoded' | null = null;
  
  constructor() {
    super();
  }

  loadSavedData(name: string, editorRef: RefObject<monaco.editor.IStandaloneCodeEditor>) {
    let sessionValue = window.sessionStorage.getItem(name);
    if (sessionValue === null) {
      let localValue = window.localStorage.getItem(name);
      if (localValue !== null) editorRef.current?.setValue(localValue);
    } else {
      editorRef.current?.setValue(sessionValue);
      window.sessionStorage.removeItem(name);
    }
  }

  onError = (ex: any, operation: string | undefined = undefined) => {
    let prefix = '';
    if (typeof operation === 'string') prefix = `${operation} failed: `;
    console.error(ex);
    if (typeof ex === 'string') {
      this.setState({ error: `${prefix}${ex}` });
    } else if (ex instanceof Error) {
      this.setState({ error: `${prefix}${ex.message}` });
    } else {
      this.setState({ error: `${prefix}${ex.toString()}` });
    }
  }

  onGlobalError = (e: ErrorEvent) => {
    console.error(e);
    this.onError(e.error);
  }

  onSourceEditorMount = (editor: monaco.editor.IStandaloneCodeEditor, _: any) => {
    editor.updateOptions({ wordWrap: 'on' });
    this._sourceEditorRef.current = editor;
    this.loadSavedData('source', this._sourceEditorRef);
  }

  onEncodedEditorMount = (editor: monaco.editor.IStandaloneCodeEditor, _: any) => {
    editor.updateOptions({ wordWrap: 'on' });
    this._encodedEditorRef.current = editor;
    this.loadSavedData('encoded', this._encodedEditorRef);
  }

  onSourceEditorChange = (value: string | undefined, _2: monaco.editor.IModelContentChangedEvent) => {
    if (globalState.saveContent && typeof value === 'string') window.localStorage.setItem('source', value);
    if (this._autoEditing === 'source') return;
    if (globalState.autoEncode) {
      this.onEncodeButtonClick();
    }
  }

  onEncodedEditorChange = (value: string | undefined, _2: monaco.editor.IModelContentChangedEvent) => {
    if (globalState.saveContent && typeof value === 'string') window.localStorage.setItem('encoded', value);
    if (this._autoEditing === 'encoded') return;
    if (globalState.autoEncode) {
      this.onDecodeButtonClick();
    }
  }

  onEncodeButtonClick = () => {
    this._autoEditing = 'encoded';
    let source = this._sourceEditorRef.current?.getValue();
    if (typeof source === 'string') {
      try {
        let encoded = encodeURIComponent(source);
        if (globalState.encodeSpaceAsPlus) encoded = encoded.replace(/%20/g, '+');
        this._encodedEditorRef.current?.setValue(encoded);
      } catch (ex) {
        this.onError(ex, 'Encoding');
      }
    }
    this._autoEditing = null;
  }

  onDecodeButtonClick = () => {
    this._autoEditing = 'source';
    let encoded = this._encodedEditorRef.current?.getValue();
    if (typeof encoded === 'string') {
      try {
        encoded = encoded.replace(/\+/g, '%20');
        this._sourceEditorRef.current?.setValue(decodeURIComponent(encoded));
      } catch (ex) {
        this.onError(ex, 'Decoding');
      }
    }
    this._autoEditing = null;
  }

  onClearSourceButtonClick = () => {
    this._sourceEditorRef.current?.setValue('');
  }

  onClearEncodedButtonClick = () => {
    this._encodedEditorRef.current?.setValue('');
  }

  onDismissErrorMessage = () => {
    this.setState({ error: null });
  }

  onResize = () => {
    if (this._sourceEditorRef !== null) this._sourceEditorRef.current?.layout({} as monaco.editor.IDimension);0
    if (this._encodedEditorRef !== null) this._encodedEditorRef.current?.layout({} as monaco.editor.IDimension);
  }

  componentDidMount() {
    window.addEventListener('resize', this.onResize);
    window.addEventListener('error', this.onGlobalError);
  }

  componentWillUnmount() {
    window.removeEventListener('resize', this.onResize);
    window.addEventListener('error', this.onGlobalError);
  }

  render() {
    const theme = useTheme();

    const sideStyle = mergeStyles(theme, {
      flexGrow: 1,
      display: 'flex',
      flexDirection: 'column',
      padding: 10
    });

    const topStyle = mergeStyles(theme, {
      display: 'flex',
      flexDirection: 'row',
      justifyContent: 'space-between',
      alignItems: 'flex-end',
      marginBottom: 10,
      width: '100%',
    });

    const buttonContainerStyle = mergeStyles(theme, {
      display: 'flex',
      flexDirection: 'row'
    });
    const leftButtonStyle = { marginRight: 10 }

    return (
      <>
        <div
          className={mergeStyles(theme, {
            position: 'absolute',
            top: 0,
            left: 0,
            width: '100%',
            minHeight: '100%',
            backgroundColor: theme.semanticColors.bodyBackground
          })}
        >
          <AboutPane />
          <AppSetup />
          <PrivacySetup onBeforeRefresh={() => {
            return new Promise<void>((resolve, _) => {
              console.log('save');
              let source = this._sourceEditorRef.current?.getValue();
              if (typeof source === 'string') window.sessionStorage.setItem('source', source);
              let encoded = this._encodedEditorRef.current?.getValue();
              if (typeof encoded === 'string') window.sessionStorage.setItem('encoded', encoded);
              console.log('save done');
              resolve();
            });
          }} />
          {this.state.error !== null &&
            <MessageBar
              className={mergeStyles(theme, {
                position: 'fixed',
                top: 50,
                left: 0,
                width: '100%',
                zIndex: 999
              })}
              messageBarType={MessageBarType.error}
              onDismiss={this.onDismissErrorMessage}
            >{this.state.error}</MessageBar>
          }
          <Header />
          <div className={mergeStyles(theme, {
            position: 'absolute',
            top: 50,
            left: 0,
            width: '100%',
            minHeight: 'calc(100% - 50px)',
            backgroundColor: theme.semanticColors.bodyBackground,
            display: 'flex',
            flexDirection: 'row'
          })}>
            <div className={sideStyle}>
              <div className={topStyle}>
                <Text variant='medium'>Source text</Text>
                <div className={buttonContainerStyle}>
                  <DefaultButton text='Clear' onClick={this.onClearSourceButtonClick} style={leftButtonStyle} />
                  <PrimaryButton text='Encode' onClick={this.onEncodeButtonClick}  />
                </div>
              </div>
              <Editor onMount={this.onSourceEditorMount} onChange={this.onSourceEditorChange} theme={theme.name === 'dark' ? 'vs-dark' : 'vs'} />
            </div>
            <div className={sideStyle}>
              <div className={topStyle}>
                <div className={buttonContainerStyle}>
                  <PrimaryButton text='Decode' onClick={this.onDecodeButtonClick} style={leftButtonStyle} />
                  <DefaultButton text='Clear' onClick={this.onClearEncodedButtonClick} />
                </div>
                <Text variant='medium'>URL-encoded text</Text>
              </div>
              <Editor onMount={this.onEncodedEditorMount} onChange={this.onEncodedEditorChange} theme={theme.name === 'dark' ? 'vs-dark' : 'vs'} />
            </div>
          </div>
        </div>
      </>
    );
  }
}