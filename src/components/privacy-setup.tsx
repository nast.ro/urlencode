import { Checkbox, Panel, PrimaryButton, Spinner, SpinnerSize, Text, Link } from '@fluentui/react';
import { Component, Fragment, h } from 'preact';

export default class PrivacySetup extends Component<{ onBeforeRefresh: () => void }, { visible: boolean, loading: boolean, changed: boolean }> {
  state = {
    visible: false,
    loading: false,
    changed: false
  };

  constructor() {
    super();
  }

  onHashChange = () => {
    let hash = window.location.hash;
    if (hash.startsWith('#')) hash = hash.substring(1);
    if (hash.toLowerCase() === 'privacysetup') {
      this.setState({ visible: true });
    } else {
      this.setState({ visible: false, changed: false, loading: false });
    }
  }

  componentDidMount() {
    window.addEventListener('hashchange', this.onHashChange);
    if (window.localStorage.getItem('allow-analytics') === null) {
      // Not configured yet, show the panel and write default settings
      window.location.hash = 'privacysetup';
      window.localStorage.setItem('allow-analytics', 'true');
    }
    this.onHashChange();
  }

  componentWillUnmount() {
    window.removeEventListener('hashchange', this.onHashChange);
  }

  onDismiss = () => {
    if (this.state.loading) return;
    if (this.state.changed) {
      this.setState({ loading: true }, () => {
        let url = new URL(window.location.toString());
        url.hash = '';
        this.props.onBeforeRefresh();
        window.location.href = url.href;
      });
    } else {
      window.history.back();
    }
  }

  onAnalyticsChanged = (_: React.FormEvent<HTMLElement | HTMLInputElement> | undefined, checked: boolean | undefined) => {
    window.localStorage.setItem('allow-analytics', checked ? 'true' : 'false');
    this.setState({ changed: true });
  }

  renderFooter = () => {
    if (this.state.loading) return null;
    return (
      <>
        <PrimaryButton onClick={this.onDismiss}>Close</PrimaryButton>
      </>
    );
  }

  render() {
    let analyticsStorageValue = window.localStorage.getItem('allow-analytics');
    let analyticsEnabled = analyticsStorageValue !== 'false';
    if (analyticsStorageValue === null) {
      // this will force the setting to be saved on first load (when setting does not exist)
      this.state.changed = true;
    }
    return (
      <Panel
        headerText='Privacy setup'
        isOpen={this.state.visible}
        onDismiss={this.onDismiss}
        onRenderFooterContent={this.renderFooter}
        hasCloseButton={!this.state.loading}
      >
        {this.state.loading ? (
          <Spinner size={SpinnerSize.large} />
        ) : (
          <>
            <p><Text variant='medium'>This site uses Plausible Analytics, a privacy-conscious analytics service.</Text></p>
            <Checkbox label="Enable Plausible Analytics" checked={analyticsEnabled} onChange={this.onAnalyticsChanged} />
            <p><Text variant='medium'>
              More information:<br />
              <Link href='https://plausible.io/data-policy' target='_blank'>Plausible Analytics' data policy</Link><br />
              <Link href='https://nast.ro/privacy/?source=urlencode' target='_blank'>Privacy info for this site</Link>
            </Text></p>
            <p><Text variant='medium'>Your inputs may be saved on your device, but they are never sent outside of it.</Text></p>
          </>
        )}
      </Panel>
    );
  }
}