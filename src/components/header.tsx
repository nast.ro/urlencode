import { FunctionalComponent, h } from 'preact';

import { IconButton, useTheme, mergeStyles } from '@fluentui/react';

const Header: FunctionalComponent  = () => {
  const theme = useTheme();
  return (
    <div className={mergeStyles(theme, {
      display: 'flex',
      justifyContent: 'center',
      flexDirection: 'column',
      position: 'fixed',
      top: 0,
      left: 0,
      height: 50,
      width: '100%',
      backgroundColor: theme.semanticColors.primaryButtonBackground,
      zIndex: 1000
    })}>
      <span className={mergeStyles(theme, {
        fontSize: theme.fonts.medium.fontSize,
        color: theme.semanticColors.primaryButtonText,
        fontWeight: 'bold',
        userSelect: 'none',
        textDecoration: 'none',
        paddingLeft: 20
      })}>URL encoder/decoder</span>
      <IconButton
        className={mergeStyles(theme, {
          position: 'absolute',
          top: 0,
          right: 50,
          width: 50,
          height: 50,
          cursor: 'pointer',
          color: theme.semanticColors.primaryButtonText,
          fontSize: '40px',
          selectors: {
            ':hover': {
              color: theme.semanticColors.primaryButtonTextHovered,
              backgroundColor: theme.semanticColors.primaryButtonBackgroundHovered
            },
            ':active': {
              color: theme.semanticColors.primaryButtonTextPressed,
              backgroundColor: theme.semanticColors.primaryButtonBackgroundPressed
            }
          }
        })}
        iconProps={{
          iconName: 'gear',
          imageProps: {
            maximizeFrame: true,
            width: 45
          }
        }}
        href="#setup"
        title="Settings"
        ariaLabel="Settings"
        onRenderMenuIcon={() => null}
      />
      <IconButton
        className={mergeStyles(theme, {
          position: 'absolute',
          top: 0,
          right: 0,
          width: 50,
          height: 50,
          cursor: 'pointer',
          color: theme.semanticColors.primaryButtonText,
          fontSize: '40px',
          selectors: {
            ':hover': {
              color: theme.semanticColors.primaryButtonTextHovered,
              backgroundColor: theme.semanticColors.primaryButtonBackgroundHovered
            },
            ':active': {
              color: theme.semanticColors.primaryButtonTextPressed,
              backgroundColor: theme.semanticColors.primaryButtonBackgroundPressed
            }
          }
        })}
        menuProps={{
          items: [
            {
              key: 'about',
              text: 'About',
              iconProps: { iconName: 'circle-info' },
              href: '#about'
            },
            {
              key: 'privacySetup',
              text: 'Change analytics preferences',
              iconProps: { iconName: 'shield-halved' },
              href: '#privacysetup'
            },
            {
              key: 'privacy',
              text: 'Privacy info',
              iconProps: { iconName: 'user-shield' },
              href: 'https://nast.ro/privacy/?source=urlencode',
              target: '_blank'
            },
            {
              key: 'source',
              text: 'View source',
              iconProps: { iconName: 'code' },
              href: 'https://gitlab.com/nast.ro/urlencode',
              target: '_blank'
            },
          ],
          directionalHintFixed: true
        }}
        iconProps={{
          iconName: 'info',
          imageProps: {
            maximizeFrame: true,
            width: 45
          }
        }}
        title="About &amp; privacy"
        ariaLabel="About &amp; privacy"
        onRenderMenuIcon={() => null}
      />
    </div>
  );
}

export default Header;
