type GlobalState = {
  autoEncode: boolean,
  saveContent: boolean,
  encodeSpaceAsPlus: boolean
}

let globalState = {
  autoEncode: window.localStorage.getItem('auto-encode') === 'true',
  saveContent: window.localStorage.getItem('save-content') !== 'false',
  encodeSpaceAsPlus: window.localStorage.getItem('encode-space-as-plus') === 'true'
}

export type { GlobalState };

export default globalState;